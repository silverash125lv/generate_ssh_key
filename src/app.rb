require 'sequel'
require 'securerandom'

PATH = "db/data.db"

DB = Sequel.sqlite(PATH)

unless DB.table_exists?(:keys)
    DB.create_table :keys do
        primary_key :id 

        String :key_id, unique: true, null: false, size: 37

        String :host, null: false 

        String :email, null: false 

        DateTime :created_at, null: false

    end
end

puts "Qual o email?"
email = gets()

puts "Qual é o nome do Computador?"
hst = gets()

tms = Time.now()

puts 'Digite a senha'
pass = gets()

table = DB[:keys]

table.insert(
    key_id: SecureRandom.uuid.to_s, 
    host: hst,
    email: email,
    created_at: tms.to_i
)

puts "Table Rules: #{table.count}"

system "ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519  -C '#{hst}: #{email} #{tms}'"
